#!/bin/bash
# golem.sh
# A script to act as basic 'AI' for a golem in D&D
# By Andrew Patterson

# Set initial stuff
rolld20 ()
{
	shuf -i 1-20 -n 1
}

rolld4 ()
{
	shuf -i 1-4 -n 1
}

rolld6 ()
{
	shuf -i 1-6 -n 1
}



#Golem Health setting
golem_health=110
declare -i golem_full=$golem_health

#Player health setting, for aggro purposes
declare -i monk_health=35
declare -i paladin_health=29
declare -i druid_health=29
declare -i wizard_health=25

# Setting other numbers so no errors
declare -i wizard_totalhit=0
declare -i wizard_totalheal=0
declare -i wizardx=0
declare -i wizardy=0
declare -i paladin_totalhit=0
declare -i paladin_totalheal=0
declare -i paladinx=0
declare -i paladiny=0
declare -i monk_totalhit=0
declare -i monk_totalheal=0
declare -i monkx=0
declare -i monky=0
declare -i druid_totalhit=0
declare -i druid_totalheal=0
declare -i druidx=0
declare -i druidy=0

#Setting initial $PLAYER_totaldam to 1, so no NaN
declare -i monk_totaldam=1
declare -i paladin_totaldam=1
declare -i druid_totaldam=1
declare -i wizard_totaldam=1


. ghelp

. turn

. char


move () {
	read -p "Set x y coordinates for golem " golem_x golem_y
}

# Because it isn't finished yet, 
echo "Not finished yet!"

if [ ! -n "$1" ]
then
	echo "Please put in the golem's stats in the form"
	echo " \"golem x y [health] [AC]\""
	echo "Options in brackets are optional."
	exit 7
fi

declare -i golem_x=$1
declare -i golem_y=$2
if [ -n "$3" ]
then
	declare -i golem_health=$3
	declare -i golem_full=$golem_health
fi
if [ -n "$4" ]
then
	declare -i golem_ac=$4
fi

while [ ! "$golem_health" == 0 ]
do
	declare -i golem_half
	let golem_half=$golem_health*2
	declare -i golem_quarter
	let golem_quarter=$golem_health*4
	if [[ "$golem_half" -gt "$golem_full" ]]
	then
		prompt='=)'
	elif [[ "$golem_quarter" -lt "$golem_full" ]] && [[ "$golem_health * 4" -gt "$golem_full" ]]
	then
		prompt='=|'
	else
		prompt='=('
	fi
	read -p "$prompt " read1 
	$read1
done
echo "The golem is dead!  Congratulations!  Here's 40xp for everyone."

